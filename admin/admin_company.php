<?php
    include "../lib/header.php";
    include "../database.php";
    $sql = 'SELECT * FROM tbl_company';

            $stat = $conn->prepare($sql);
            $stat->execute();
            $list = $stat->fetchall(PDO::FETCH_OBJ);

        if(isset($_GET['id'])){

                $id = $_GET['id'];
                $sql = "DELETE FROM tbl_company WHERE company_id=:id";
                $stat = $conn->prepare($sql);
                $stat->execute([
                                ':id'=>$id]
                                );    
                header("Location: admin_company.php");
            }

        if(isset($_POST['submit'])){
        $errMSG = "";

        $company_name = $_POST['Company_Name'];
        $company_address = $_POST['Company_Address'];

        if(empty($company_name)){
            $Cname_errMSG = "Please enter Company name.";
            // echo $errMSG;
        }
        else if(empty($company_address))
        {
            $Cadd_errMSG = "Please enter your Company address.";
            // echo $Cadd_errMSG;
        }

        if(empty($Cname_errMSG) && empty($Cadd_errMSG)){
         
        $sql = 'INSERT INTO tbl_company(company_name,address) VALUES(:comp_name,:comp_address)';

        $stat = $conn->prepare($sql);

        $stat->execute([
            ':comp_name'    => $company_name,
            ':comp_address' => $company_address
                        ]);
        
        header("Refresh:0");
        // header("Location: admin_company.php");
        }
    }

     if(isset($_POST['search'])){
            $searrch_name = $_POST['Company_Name'];
            // $search = "p";
            $sql = "SELECT * FROM tbl_company WHERE company_name LIKE :search";
            $stat = $conn->prepare($sql);
            $stat->execute([':search' => '%'.$searrch_name.'%']);
            $list = $stat->fetchall(PDO::FETCH_OBJ);
            
             // print_r($list);

         }
?>


<div class="row">
    <div class="col-md-12">
        <span>    

    <form action="" method="post">
                        <center>
                            <div class="col-md-12">
                                 <div class="col-md-2"></div>
                                 <div class="comp col-md-12">
                                    <h3>Add Company</h3>
                           <ul>
                                <li>
                                    <label for="Company Name">Company Name: </label>
                                    <input type="text" name="Company_Name" id="Company Name" placeholder="Company Name"> 
                            <?php if(isset($Cname_errMSG)){ echo $Cname_errMSG; }?> </li>

                                <li><label for="Company Address">Company Address: </label>
                                    <input type="text" name="Company_Address" id="Company Address" placeholder="Company Address"> 
                            <?php if(isset($Cadd_errMSG)){echo $Cadd_errMSG;}?> 
                            </li>
                       
                                <li><input type="submit" name="submit" id="submit" value="Add"   
                                </li>
                            </ul>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </center>       
    </form>
 
 <form action="admin_company.php" method="post">
    <div class="row">
        <div class="comp col-md-12">
            <div class="col-md-6"></div>
            <div class="col-md-6"> 
                <input type="text" name="Company_Name" id="Company_Name" placeholder="Company_Name"> 
                <input type="submit" class="btn" name="search" value ="Search"> 
            </div>
        </div>
    </div>
 </form>
 

            <div class="container-fluid">
                <div class="row">
                <div class="col-md-12">
                <form action="admin_company.php" method="get" id="<?= $data->company_id; ?>">
                    <table  style="width: 100%; border: 1px solid black;" >
                            <tr>
                                <th>Company Name</th>
                                <th>Company Address</th>
                                <th>Action</th>
                            </tr>
                        <?php foreach($list as $data): ?>   
                            <tr >
                                <td>
                                    <label for="username" ><?= $data->company_name; ?></a></label> 
                                </td>
                                <td>
                                    <label for="username" ><?= $data->address; ?></label>
                                </td>

                                <td>
                        <a class="btn btn-primary" href="company_edit.php?id=<?= $data->company_id; ?>">Edit</a> 

                         <input type="hidden" name="id" value="<?= $data->company_id; ?>" /><input type="submit" id="<?= $data->company_id; ?>" class="btn" name="Delete" value ="Delete"> 
                                </td>
                        </tr>
                        <?php endforeach; ?>
                        <br>  
                    </table>
                </form>
                 </div>
            </div>
        </div>



                        </span>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>

        
 <?php include "../lib/footer.php";?>