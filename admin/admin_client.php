 
<?php
    include "../lib/header.php";
    include "../database.php";
    $sql = "
        SELECT cmpny.* , clnt.* FROM tbl_company cmpny JOIN tbl_client clnt ON cmpny.company_id = clnt.company_id;
        SELECT * FROM tbl_client;
        ";
            $stat = $conn->prepare($sql);
            $stat->execute();
            $list = $stat->fetchall(PDO::FETCH_OBJ);

     // $sql2 = 'SELECT * FROM tbl_client';
     //        $stat = $conn->prepare($sql2);
     //        $stat->execute();
     //        $list2 = $stat->fetchall(PDO::FETCH_OBJ);


        //company_selection
        $sql = "SELECT * FROM tbl_company";
        $stat = $conn->prepare($sql);
        $stat->execute();
        $data = $stat->fetchall(PDO::FETCH_OBJ);



        //add_client
        if(isset($_POST['add'])){
            $contact_name   = $_POST['contact_name'];
            $contact_num    = $_POST['contact_num'];
            $company_name   = $_POST['company_name'];
            $user_id        = "";
            $userprofile    = "";       
                    $sql = "SELECT * FROM tbl_company WHERE company_name=:company_name";
                    $stat = $conn->prepare($sql);
                    $stat->execute([':company_name'=> $company_name]);
                    $v = $stat->fetch(PDO::FETCH_OBJ);
                    $company_id = $v->company_id;
           
                    $sql = 'INSERT INTO tbl_client(client_name,contact_num,company_id,user_id,image) VALUES(:name,:contact_num,:company_id,:user_id,:image)';
                    $stat = $conn->prepare($sql);
                    $stat->execute([':name'=> $contact_name, ':contact_num' => $contact_num, ':company_id' => $company_id, ':user_id' => $user_id, ':image' => $userprofile]);
                    header("Refresh: 0");
        }

         if(isset($_POST['search'])){
            $searrch_name = $_POST['Client_Name'];
            // $search = "p";

            $sql= "SELECT cmpny.* , clnt.* FROM tbl_company cmpny JOIN tbl_client clnt ON cmpny.company_id = clnt.company_id WHERE client_name LIKE :search";
            // $sql = "SELECT * FROM tbl_client
            $stat = $conn->prepare($sql);
            $stat->execute([':search' => '%'.$searrch_name.'%']);
            $list = $stat->fetchall(PDO::FETCH_OBJ);
            
             // print_r($list);

         }
 
?>

    <form action="" method="post">
        <center>
            <div class="col-md-12">
                 <div class="col-md-2"></div>
                 <div class="comp col-md-12">
                    <h3>Add Contacts</h3>
                   <ul>
                        <li>
                            <label for="Contact Name">Contact Name: </label>
                            <input type="text" name="contact_name" id="Contact Name" placeholder="Contact Name"> 

                         <?php if(isset($Cname_errMSG)){ echo $Cname_errMSG; }?> 
                            </li>
                        <li><label for="Contact Number">Contact Number:</label>
                            <input type="text" name="contact_num" id="Contact Number" placeholder="Contact Number"> 
                                <?php if(isset($Cadd_errMSG)){echo $Cadd_errMSG;}?>
                            </li>
                        <li>
                            <label>Company Name:</label>
                                <select name="company_name">
                                    <option>choose company</option>
                                    <?php foreach($data as $value):?>
                                    <option><?= $value->company_name; ?></option>
                                    <?php endforeach; ?>
                                  </select>
                        </li>
                        <li><input type="submit" name="add" id="submit" value="Add Client"></li>
                    </ul>
                 </div>
                 <div class="col-md-2"></div>
            </div>
             
        </center>       
    </form>

    <form action="admin_client.php" method="post">
    <div class="row">
        <div class="comp col-md-12">
            <div class="col-md-6"></div>
            <div class="col-md-6"> 
                <input type="text" name="Client_Name" id="Client_Name" placeholder="Client_Name"> 
                <input type="submit" class="btn" name="search" value ="Search"> 
            </div>
        </div>
    </div>
 </form>


            <div class="container-fluid">
                <div class="row">
                <div class="col-md-12">
                <form action="" method="get" id="<?= $data->company_id; ?>">
                    <table  style="width: 100%; border: 1px solid black;" >
                            <tr>
                                <th>Client Name</th>
                                <th>Contact Number</th>
                                <th>Company Name</th>
                                <th>Action</th>
                            </tr>
                                    
                        <?php foreach($list as $data2): ?>   
                            <tr >
                                <td>
                                    <label for="username" ><?= $data2->client_name; ?></a></label> 
                                </td>
                                <td>
                                    <label for="contact_number" ><?= $data2->contact_num; ?></label>
                                </td>
                          
                                <td>
                                    <label for="Company_name" ><?= $data2->company_name; ?></label>
                                </td>
                        
                                <td>
                            
                            <!-- Edit -->
                            <button class="btn btn-primary"><a href="contacts_edit.php?client_id=<?= $data2->client_id;?>">Edit</a></button>
                            <!-- Delete -->
                            <button  class="btn btn-primary" name="Delete"><a href="client_delete.php?client_id=<?= $data2->client_id;?>">Delete</a></button>
                        </tr>
                        <?php endforeach; ?>
                       
                        <br>  
                    </table>
                </form>
                 </div>
            </div>
        </div>



                        </span>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
 <?php include "../lib/footer.php";?>
